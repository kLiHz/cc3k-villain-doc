#ifndef FLOOR_HPP
#define FLOOR_HPP

#include "Player.hpp"

#include <string>
#include <vector>
#include <iostream>

class Floor {
private:
    static const int width = 10;
    static const int height = 5;
    std::vector<std::string> terrian;

    // 目前只有一个主角
    Player pc;

    // 如果有多个其他角色，可使用容器存储，如
    // std::vector<Character> characters;

public:
    Floor();
    ~Floor() = default;
    bool is_avaliable_pos(Point pos) {
        return terrian[pos.row][pos.col] == '.';
    }
    void move_player(Point new_pos) {
        pc.set_pos(new_pos);
    }
    auto get_player_pos() {
        return pc.get_pos();
    }
    void print_to(std::ostream &os);
};

#endif  // FLOOR_HPP
